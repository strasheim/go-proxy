package main

import (
	"log"
	"net/http"
	"os"

	"github.com/goproxy/goproxy"
)

func main() {
	privateDomain := os.Getenv("repo")
	log.Println("[INFO]", "starting the go module proxy for", privateDomain)
	g := goproxy.Goproxy{}
	dirCacher := goproxy.DirCacher("/tmp/goproxycache")
	g.Cacher = dirCacher
	g.GoBinEnv = append(
		os.Environ(),
		"GOPROXY=https://proxy.golang.org,direct",
		"GOPRIVATE="+privateDomain,
	)
	err := http.ListenAndServe(":8081", &g)
	if err != nil {
		log.Println("[ERROR]", "serving go modules failed", err)
	}
}
